# SKA SDP PSS Send and Receive Environment

## **About**  
Set up SDP PSS Send and Receive Environment
Build of a containerized send-receive environment

The system is availabe as a single Virtualbox container 
http://tirgo.arcetri.inaf.it/testvectors/containers

## **Installation**  
Install the project dependencies:  
* minikube [https://kubernetes.io/docs/tasks/tools/install-minikube/]  
* docker [https://www.docker.com/]  
  

Clone this repository 
  ```git clone https://gitlab.com/SKA-TDT/prototypes```
