# TDT Bridging Prototypes

## **About**
In this area are stored The evolutionary prototypes developed by 
TDT grop during SKA Bridging pahse

## **Contents**

* *PSS_SDP_communication*  PSS SDP communication prototype code

* *MAAS_Ansible_deployment* code to mass deploy TDT software
