#!/bin/bash
#
# Create user pss and populate .ssh directory
#

# get some files
cd /tmp
wget -v http://10.98.68.250/store/pss_ssh.tgz
wget -v http://10.98.68.250/store/authorized_keys

# create user
sudo /sbin/useradd pss
sudo chmod ugo+rx /home/pss
# manage ssh keys
cd /home/pss
sudo mkdir /home/pss/.ssh
sudo tar xvf /tmp/pss_ssh.tgz --directory /home/pss
sudo mv /home/pss/config .ssh
sudo mv /tmp/authorized_keys .ssh
sudo chmod go-rw /home/pss/.ssh/authorized_keys
sudo chown -R pss.pss /home/pss/.ssh
# create src dir
sudo mkdir /home/pss/src
sudo chown -R pss.pss /home/pss/src

# cleaning
rm /tmp/pss_ssh.tgz
