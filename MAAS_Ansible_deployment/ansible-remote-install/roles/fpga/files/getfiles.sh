#!/bin/bash
#
# fetch some util files
#

# get some files
cd /tmp
# Driver
wget -cq http://10.98.68.250/store/FPGA/a10pl4_16.1.2_bsp.tar
# Altera OpenCL
wget -cq http://10.98.68.250/store/FPGA/aocl-rte-16.1.0-1.x86_64.rpm
# Custom bash settings
wget -v http://10.98.68.250/store/FPGA/etc_profile.d_fpga_custom.sh
chmod +x etc_profile.d_fpga_custom.sh

# cleaning
#rm /tmp/pss_ssh.tgz
