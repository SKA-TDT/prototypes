#!/bin/bash
#
# Install some util files
#

# miscellaneous operations
cd /tmp
# make kernel source available to install script 
cd /usr/src/kernels/
sudo /usr/bin/ln -sfb /usr/src/kernels/current \
    /usr/src/kernels/3.10.0-862.3.2.el7.x86_64
#sudo /usr/bin/ln -sfb /usr/src/kernels/3.10.0-957.10.1.el7.x86_64 \
#    /usr/src/kernels/3.10.0-862.3.2.el7.x86_64

# cleaning
#rm /tmp/pss_ssh.tgz
