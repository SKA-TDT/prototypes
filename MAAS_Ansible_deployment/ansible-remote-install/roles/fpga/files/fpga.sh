#!/bin/bash
#
# Install cuda 9.
#

# already got some files 
cd /tmp

# Deploy driver

sudo rpm -Uvh /tmp/aocl-rte-16.1.0-1.x86_64.rpm

# cleaning
rm /tmp/aocl-rte-16.1.0-1.x86_64.rpm

# add aocl path
sudo cp /root/.bashrc /tmp
sudo chown centos /tmp/.bashrc
if ! grep -qe "/opt/altera/aocl-rte" /root/.bashrc; then
     echo "# Adding aocl path" >> /tmp/.bashrc
     echo "export PATH=/opt/altera/aocl-rte/bin${PATH:+:${PATH}} " >> /tmp/.bashrc
     sudo cp  /tmp/.bashrc /root/
fi

# to be executed on bash start-up
sudo chown root /tmp/etc_profile.d_fpga_custom.sh
sudo mv /tmp/etc_profile.d_fpga_custom.sh /etc/profile.d/fpga_custom.sh

# Update the correct board
cd /tmp
tar -xvf a10pl4_16.1.2_bsp.tar
sudo mv 16.1.2_bsp/a10pl4 /opt/altera/aocl-rte/board/
sudo rm -r 16.1.2_bsp

# enable libraries
sudo /sbin/ldconfig


