#!/bin/bash
#
# Install cuda 9.
#

# get some files
cd /tmp
wget -cq http://10.98.68.250/store/CUDA/cuda_9.2.148_396.37_linux.run

# Deploy CUDA

sudo bash /tmp/cuda_9.2.148_396.37_linux.run --silent --driver --toolkit   \
    --samples --samplespath=/home/pss/Samples                             \
    --kernel-source-path=/usr/src/kernels/current                         \
    --verbose

# cleaning
rm /tmp/cuda_9.2.148_396.37_linux.run


# add cuda path
if ! grep -qe "local/cuda/bin" /root/.bashrc; then
     sudo echo "# Adding cuda path" >> /root/.bashrc
     sudo echo "export PATH=/usr/local/cuda/bin${PATH:+:${PATH}}" >> /root/.bashrc
fi   

# enable libraries
sudo /sbin/ldconfig
