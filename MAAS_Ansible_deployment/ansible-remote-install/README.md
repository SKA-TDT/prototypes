## Remote install of protonip.

- Requires Ansible 1.2 or newer
- Expects CentOS/RHEL 7.x hosts

This playbook deploys a minimal environment for SKA-PSS software
development.

It is intended to be roon from the toor user in skapss-maas machine
Requires the set-up of id_rsa_maas ssh identity

To use it, first edit the "hosts" inventory file to contain the
hostnames of the machines on which you want protonip SW deployed, 

# and edit the group_vars/protonip file to set any protonip configuration parameters you need.

Then run the playbook, like this:

	ansible-playbook -i hosts -u centos protonip.yml

When the playbook run completes, you should be able to log in the deployed
machines with username 'pss' via the ssh keys distributed before.

This is a very simple playbook and could serve as a starting point for more
complex protonip set-up.

* Prerequisites

On the master machine ( 10.98.68.250) we need Apache2 installed. On the 
http://10.98.68.250/store it should be present the installation files
(copies in http://tirgo.arcetri.astro.it/testvectors)
