#!/bin/bash
#
# Install some util files
#

# get some files
cd /tmp
# blacklist nouveau
wget -v http://10.98.68.250/store/blacklist.conf
sudo chown root blacklist.conf
sudo mv blacklist.conf /etc/modprobe.d/
# add cuda libraries
wget -v http://10.98.68.250/store/cuda.conf
sudo chown root cuda.conf
sudo mv cuda.conf /etc/ld.so.conf.d/
# fast fake
wget -v http://10.98.68.250/store/fake
chmod +x fake
sudo chown root fake
sudo mv fake /usr/local/bin
# vectors util
wget -v http://10.98.68.250/store/get_testvectors.sh
chmod +x get_testvectors.sh
sudo chown root get_testvectors.sh
sudo mv get_testvectors.sh /usr/local/bin
# device info & listing
wget -v http://10.98.68.250/store/deviceQuery
chmod +x deviceQuery
sudo chown root deviceQuery
sudo mv deviceQuery /usr/local/bin


# cleaning
#rm /tmp/pss_ssh.tgz
