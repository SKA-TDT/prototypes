#!/bin/bash
#
# Install cuda 8.
#

# get some files
cd /tmp
wget -cq http://10.98.68.250/store/CUDA/cuda_8.0.61_375.26_linux-run
wget -cq http://10.98.68.250/store/CUDA/NVIDIA-Linux-x86_64-440.44.run

# install nvidia driver
#

sudo bash /tmp/NVIDIA-Linux-x86_64-440.44.run --silent

# Deploy CUDA

sudo bash /tmp/cuda_8.0.61_375.26_linux-run --silent  --toolkit           \
    --samples --samplespath=/home/pss/Samples                             \
    --kernel-source-path=/usr/src/kernels/current                         \
    --verbose

# cleaning
rm /tmp/cuda_8.0.61_375.26_linux-run


# add cuda path
if ! grep -qe "local/cuda/bin" /root/.bashrc; then
     sudo echo "# Adding cuda path" >> /root/.bashrc
     sudo echo "export PATH=/usr/local/cuda/bin${PATH:+:${PATH}}" >> /root/.bashrc
fi   

# enable libraries
sudo /sbin/ldconfig
