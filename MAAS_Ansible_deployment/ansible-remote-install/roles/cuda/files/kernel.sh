#!/bin/bash
#
# Create a link to kernel includes
#

# miscellaneous operations
cd /tmp
# make kernel source available to install script 
sudo yum install -y kernel-devel-`/usr/bin/uname -r`
cd /usr/src/kernels/
#sudo /usr/bin/ln -s /usr/src/kernels/3.10.0-957.10.1.el7.x86_64 
sudo /usr/bin/ln -s -b /usr/src/kernels/`/usr/bin/uname -r` \
    /usr/src/kernels/current

# cleaning
#rm /tmp/pss_ssh.tgz
